jQuery(document).ready(function($) {
	
	const yecm_events = [{
		start: '2019-04-20 1:30',
		end: '2019-04-22 12:30',
		title: 'Event 1',
		url: '#',
		class: 'custom-class',
		color: '#5abc',
		data: { 
			test: 'text',
		}
	},{
		start: '2019-04-21 1:30',
		end: '2019-04-24 12:30',
		title: 'Event 2',
		url: '#',
		class: 'custom-class',
		color: '#000',
		data: { }
	},{
		start: '2019-04-24 17:30',
		end: '2019-04-30 17:30',
		title: 'Event 3',
		url: '#',
		class: 'custom-class',
		color: '#000',
		data: {  }
	}];
	jQuery('#your-events-calendar').equinox({
		events: yecm_events,
	});

});