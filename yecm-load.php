<?php

/**
	* Plugin Name: Your Events Calendar Manger
	* Plugin URI:  
	* Description: This is a plugin that manage your events.
	* Version:     20190415
	* Author:      Melvin Belenario
	* Author URI:  
	* License:     GPL2
	* License URI: https://www.gnu.org/licenses/gpl-2.0.html
	* Text Domain: yecm
	* Domain Path: /languages
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}


if ( ! class_exists( 'YECM' ) ) {

	final class YECM{
		private static $_instance = null;

		/**
		 	*  constructor.
	 	*/
		public function __construct() {
			$this->define_constants();
			$this->includes();
			$this->init_hooks();
		}
		public function init_hooks() {
			// plugin loaded
			add_action( 'plugins_loaded', array( $this, 'loaded' ) );
		}
		public function loaded() {
			do_action( 'yecm_init', $this );
		}

		/**
		 	* Define Plugins Constants
	 	*/
		public function define_constants() {
			$this->set_define( 'YECM_PATH', plugin_dir_path( __FILE__ ) );
			$this->set_define( 'YECM_URI', plugin_dir_url( __FILE__ ) );
			$this->set_define( 'YECM_INC', YECM_PATH . 'inc/' );
			$this->set_define( 'YECM_INC_URI', YECM_INC . 'inc/' );
			$this->set_define( 'YECM_ASSETS_URI', YECM_URI . 'assets/' );
			$this->set_define( 'YECM_LIB_URI', YECM_INC_URI . 'libraries/' );
			$this->set_define( 'YECM_VER', '20190415' );
			$this->set_define( 'YECM_MAIN_FILE', __FILE__ );
		}
		public function set_define( $name = '', $value = '' ) {
			if ( $name && ! defined( $name ) ) {
				define( $name, $value );
			}
		}

		/**
		 * Include files.
		 */
		public function includes() {

			$this->_include( 'inc/class.yecm-main.php' );

			if ( is_admin() ) {
				$this->_include( 'inc/admin/class.yecm-admin.php' );
			}

		}
		public function _include( $file = null ) {
			if ( is_array( $file ) ) {
				foreach ( $file as $key => $f ) {
					if ( file_exists( YECM_PATH . $f ) ) {
						require_once YECM_PATH . $f;
					}
				}
			} else {
				if ( file_exists( YECM_PATH . $file ) ) {
					require_once YECM_PATH . $file;
				} elseif ( file_exists( $file ) ) {
					require_once $file;
				}
			}
		}

		public static function instance() {
			if ( ! empty( self::$_instance ) ) {
				return self::$_instance;
			}

			return self::$_instance = new self();
		}
	}

	if( ! function_exists( 'YECM' ) ) {

		function YECM() {
			return YECM::instance();
		}

	}
	YECM();
}

$GLOBALS['YECM'] = YECM();