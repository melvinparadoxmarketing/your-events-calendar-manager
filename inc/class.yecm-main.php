<?php

defined( 'ABSPATH' ) || exit;

class YECM_Main {

	public function __construct() {

		$this->_includes();

		add_action( 'wp_enqueue_scripts', array( $this, 'yecm_enqueue_front' ) );
	}

	private function _includes() {
		include( YECM_PATH . 'inc/class.yecm-custom-post-type.php' );
		include( YECM_PATH . 'inc/class.yecm-shortcodes.php' );
	}

	public function yecm_enqueue_front(){
		wp_enqueue_style(
			'yecm_equinox_style',
			YECM_ASSETS_URI.'equinox/equinox.css',
			array(),
			YECM_VER,
			'all'
		);
		wp_enqueue_style(
			'yecm_front_style',
			YECM_ASSETS_URI.'css/yecm-front-style.css',
			array(),
			YECM_VER,
			'all'
		);
		wp_enqueue_script(
			'yecm_moment_script',
			YECM_ASSETS_URI.'js/moment.min.js',
			array('jquery'),
			YECM_VER,
			false
		);
		wp_enqueue_script(
			'yecm_equinox_script',
			YECM_ASSETS_URI.'equinox/equinox.min.js',
			array('jquery'),
			YECM_VER,
			false
		);
		wp_enqueue_script(
			'yecm_front_script',
			YECM_ASSETS_URI.'js/yecm-front-script.js',
			array('jquery'),
			YECM_VER,
			false
		);
	}

}

new YECM_Main();
