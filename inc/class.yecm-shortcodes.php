<?php

defined( 'ABSPATH' ) || exit;

class YECM_Shortcodes {

	public function __construct() {

		add_shortcode( 'your-events-calendar', array( $this, 'your_events_calendar_shortcode' ) );
	}

	
	function your_events_calendar_shortcode( $atts ) {
		ob_start();
		?>

		<div id="your-events-calendar"></div>


		<?php
		return ob_get_clean();
	}
}

new YECM_Shortcodes();
