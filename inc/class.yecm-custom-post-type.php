<?php

defined( 'ABSPATH' ) || exit;


class YECM_Custom_Post_Types {

	public function __construct() {
		add_action( 'init', array( $this, 'register_event_post_type' ) );
	}

	public function register_event_post_type() {
		// post type
		$labels = array(
			'name'                => _x( 'Events', 'Post Type General Name', 'yecm' ),
			'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'yecm' ),
			'menu_name'           => __( 'Events', 'yecm' ),
			'parent_item_colon'   => __( 'Parent Event', 'yecm' ),
			'all_items'           => __( 'All Events', 'yecm' ),
			'view_item'           => __( 'View Event', 'yecm' ),
			'add_new_item'        => __( 'Add New Event', 'yecm' ),
			'add_new'             => __( 'Add New Event', 'yecm' ),
			'edit_item'           => __( 'Edit Event', 'yecm' ),
			'update_item'         => __( 'Update Event', 'yecm' ),
			'search_items'        => __( 'Search Event', 'yecm' ),
			'not_found'           => __( 'Event Not Found', 'yecm' ),
			'not_found_in_trash'  => __( 'Events Not found in Trash', 'yecm' ),
		);
		$supports = array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields' );
		
		$args = array(
			'label'               => __( 'Events', 'yecm' ),
			'description'         => __( 'Event news and reviews', 'yecm' ),
			'labels'              => $labels,
			'supports'            => $supports,
			'hierarchical'        => false,
	        'public'              => true,
	        'show_ui'             => true,
	        'show_in_menu'        => 'yecm-setting',
	        'show_in_nav_menus'   => true,
	        'show_in_admin_bar'   => true,
	        'menu_position'       => 5,
	        'can_export'          => true,
	        'has_archive'         => true,
	        'publicly_queryable'  => true,
	        'menu_icon'			  => 'dashicons-calendar',
	        'rewrite'			  => array( 'slug' => 'event' , 'with_front' => false ),
			'capability_type'     => 'page',
		);
		register_post_type( 'yecm_event', $args );

		$labels = array(
			'name'              => _x( 'Event Categories', 'taxonomy general name', 'yecm' ),
			'singular_name'     => _x( 'Event Category', 'taxonomy singular name', 'yecm' ),
			'search_items'      => __( 'Search Event Categories', 'yecm' ),
			'all_items'         => __( 'All Event Categories', 'yecm' ),
			'parent_item'       => __( 'Parent Event Category', 'yecm' ),
			'parent_item_colon' => __( 'Parent Event Category:', 'yecm' ),
			'edit_item'         => __( 'Edit Event Category', 'yecm' ),
			'update_item'       => __( 'Update Event Category', 'yecm' ),
			'add_new_item'      => __( 'Add New Event Category', 'yecm' ),
			'new_item_name'     => __( 'New Event Category Name', 'yecm' ),
			'menu_name'         => __( 'Event Category', 'yecm' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'event-category' ),
		);

		register_taxonomy( 'yecm_event_cat', array( 'yecm_event' ), $args );

		$labels = array(
			'name'                       => _x( 'Event Tags', 'taxonomy general name', 'yecm' ),
			'singular_name'              => _x( 'Event Tag', 'taxonomy singular name', 'yecm' ),
			'search_items'               => __( 'Search Event Tags', 'yecm' ),
			'popular_items'              => __( 'Popular Event Tags', 'yecm' ),
			'all_items'                  => __( 'All Event Tags', 'yecm' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Event Tag', 'yecm' ),
			'update_item'                => __( 'Update Event Tag', 'yecm' ),
			'add_new_item'               => __( 'Add New Event Tag', 'yecm' ),
			'new_item_name'              => __( 'New Event Tag Name', 'yecm' ),
			'separate_items_with_commas' => __( 'Separate Event Tags with commas', 'yecm' ),
			'add_or_remove_items'        => __( 'Add or remove Event Tags', 'yecm' ),
			'choose_from_most_used'      => __( 'Choose from the most used Event Tags', 'yecm' ),
			'not_found'                  => __( 'No Event Tags found.', 'yecm' ),
			'menu_name'                  => __( 'Event Tags', 'yecm' ),
		);

		$args = array(
			'hierarchical'          => false,
			'labels'                => $labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
			'rewrite'               => array( 'slug' => 'event-tag' ),
		);

		register_taxonomy( 'yecm_event_tag', 'yecm_event', $args );

		
	}

}

new YECM_Custom_Post_Types();