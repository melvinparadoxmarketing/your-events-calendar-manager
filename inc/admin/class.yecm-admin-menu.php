<?php


defined( 'ABSPATH' ) || exit;

class YECM_Admin_Menu {

	public function __construct() {

		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
	}

	public function admin_menu() {
		add_menu_page( 
			__( 'YECM', 'yecm' ), 
			__( 'YECM', 'yecm' ), 
			'administrator', 
			'yecm-setting', 
			null, 
			'dashicons-calendar-alt', 
			4 
		);
		add_submenu_page(
	        'yecm-setting',
	        __( 'Event Categories', 'yecm' ),
	        __( 'Event Categories', 'yecm' ),
	        'edit_posts',
	        'edit-tags.php?taxonomy=yecm_event_cat&post_type=yecm_event',
	        false
	    );
	    add_submenu_page(
	        'yecm-setting',
	        __( 'Event Tags', 'yecm' ),
	        __( 'Event Tags', 'yecm' ),
	        'edit_posts',
	        'edit-tags.php?taxonomy=yecm_event_tag&post_type=yecm_event',
	        false
	    );
		add_submenu_page( 
			'yecm-setting', 
			__( 'Settings', 'yecm' ), 
			__( 'Settings', 'yecm' ), 
			'manage_options', 
			'yecm-event-setting', 
			array( $this, 'admin_menu_page_content' )
		);
	}

	public function admin_menu_page_content(){

		if ( ! current_user_can( 'manage_options' ) ) {
			return;
		}
		?>
		<h1><?php echo get_admin_page_title(); ?></h1>
		<?php
		if ( isset( $_GET['settings-updated'] ) ) {
			// add settings saved message with the class of "updated"
			add_settings_error( 'yecm_options_messages', 'yecm_options_message', __( 'Settings Saved', 'wporg' ), 'updated' );
		}
		// show error/update messages
		settings_errors( 'yecm_options_messages' );
		?>
		<form action="options.php" method="post">
			<?php
			// output security fields for the registered setting 
			settings_fields( 'yecm_setting' );
			// output setting sections and their fields
			do_settings_sections( 'yecm_setting' );
			// output save settings button
			submit_button( 'Save Settings' );
			?>
		</form>

		<?php
	}
}

new YECM_Admin_Menu();
