<?php

defined( 'ABSPATH' ) || exit;

class YECM_Admin {

	public function __construct() {

		$this->_includes();
	}

	private function _includes() {
		include( YECM_PATH . 'inc/admin/class.yecm-admin-fields.php' );
		include( YECM_PATH . 'inc/admin/class.yecm-admin-menu.php' );
	}

}

new YECM_Admin();
