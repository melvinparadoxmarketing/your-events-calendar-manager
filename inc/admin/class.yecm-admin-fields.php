<?php


defined( 'ABSPATH' ) || exit;

class YECM_Admin_Fields {

	public function __construct() {

		add_action( 'admin_init', array( $this, 'admin_init' ) );
	}

	public function admin_init() {

		register_setting( 'yecm_setting', 'yecm_options' );

		add_settings_section(
			'yecm_options_section_1',
			__( 'Calendar Settings', 'yecm' ),
			array( $this, 'yecm_setting_section_cb' ),
			'yecm_setting'
		);

		add_settings_field(
			'yecm_calendar_title',
			__( 'Calendar Title', 'yecm' ),
			array( $this, 'yecm_calendar_title_cb' ),
			'yecm_setting',
			'yecm_options_section_1',
			array(
				'label_for' => 'yecm_calendar_title',
			)
		);
	}

	function yecm_calendar_title_cb( $args ) {
		// get the value of the setting we've registered with register_setting()
		$options = get_option( 'yecm_options' );
		// output the field
		?>

		<input type="text" id="<?php echo esc_attr( $args['label_for'] ); ?>" name="yecm_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php if( isset($options['yecm_calendar_title'])){ echo $options['yecm_calendar_title']; } ?>">

		<?php
	}

	public function yecm_setting_section_cb( $args ) {
		?>
		<p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'Customize your calendar of events.', 'yecm' ); ?></p>
		<?php
	}

	
}

new YECM_Admin_Fields();
